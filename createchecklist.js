{/* <div class="cardDiscriptionCheckLists">
<div class="checklistName">
    <i class="fa fa-check-square"></i>
    <span>Checklist</span>
    <span>Delete</span>
</div>
<div class="progressBar">
    
    <div class="progress">
        <div class="progress-bar" role="progressbar"
            style="width: 25%; background-color: rgb(36, 154, 190);" aria-valuenow="25"
            aria-valuemin="0" aria-valuemax="100">25%</div>
    </div>
</div>
<div class="checkBoxes">
    <div class="form-check checkBox">
        <input type="checkbox" class="form-check-input" id="exampleCheck1">
        <label class="form-check-label" for="exampleCheck1">Item name</label>
    </div>
</div>
<div class="addCard">
    <i class="fa fa-plus plus"></i>
    <p>Add an Item...</p>
</div>

</div> */}

function createCheckList(name,Id) {
    const checkListContainer= document.querySelector(".cardDescriptionCheckLists");
    const checkListDiv = document.createElement("div");
    checkListDiv.classList.add = ".checklist";
    checkListDiv.id = Id;
    checkListDiv.innerHTML = `<div class="checklistName">
                                        <i class="fa fa-check-square"></i>
                                        <span class="checklistname">${name}</span>
                                        <span class = "delete">Delete</span>
                                    </div>
                                    <div class="checkBoxes">
                                    </div>
                                    <div class="addItem">
                                        <i class="fa fa-plus plus"></i>
                                        <p>Add an Item...</p>
                                    </div>`;

     checkListContainer.append(checkListDiv);
} 

export default createCheckList;