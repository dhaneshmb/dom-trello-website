
let createBoard = (name, id)=>{
    let board = document.createElement("div");
    let heading = document.createElement("div");
    let deleteIcon = document.createElement("i");
    board.classList.add("board");
    board.id = id;
    heading.classList.add("heading");
    heading.innerText = name;
    deleteIcon.classList.add("fa", "fa-trash", "deleteIcon");
    board.append(deleteIcon);
    board.append(heading);
    let boardContainer = document.querySelector(".board-container");
    boardContainer.insertBefore(board,boardContainer.lastElementChild)
}


export default createBoard;