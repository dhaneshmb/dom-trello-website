const createCard = (name,id,listId) => {
    const list = document.getElementById(listId);
    console.log(list)
    const card = document.createElement("div");
    card.classList.add("cardName");
    card.id = id;
    card.innerHTML=`<p>${name}</p>`;
    const addCardDiv = list.querySelector(".addCard");
    list.insertBefore(card,addCardDiv);
}

export default createCard;