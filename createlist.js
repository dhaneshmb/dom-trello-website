{/* <div class="list">
                <h3>List</h3>
                <div class="cardName">
                    <p>Card Name</p>
                </div>
                <div class="mb-3 Title">
                    <!-- <label for="exampleFormControlTextarea1" class="form-label">Example</label> -->
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"
                        placeholder="Enter the title"></textarea>
                    <div>
                        <button type="button" class="btn btn-outline-success button">Add Card</button>
                        <i class="fa fa-times"></i>
                    </div>
                </div>
                <div class="addCard">
                    <i class="fa fa-plus plus"></i>
                    <p>Add a Card...</p>
                </div>
            </div> */}
const createList = (name,id) => {
   const listContainer = document.querySelector(".list-container");
   const list = document.createElement("div");
   list.classList.add("list");
   list.id = id;
   list.innerHTML = `<h3>${name}</h3>
                    <div class = "deletelistIcon">
                      <i class="fa fa-trash"></i>
                    </div>
                    <div class="addCard">
                        <i class="fa fa-plus plus"></i>
                        <p>Add a Card...</p>
                    </div>`;
   
   let addListDiv  = document.querySelector(".addList");
   listContainer.insertBefore(list, addListDiv);
}

export default createList;

