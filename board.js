import createList from './createlist.js';
import createCard from './createcard.js';
import displayCard from './displaycard.js';
import createCheckList from './createchecklist.js';
import createItem from './createitem.js';
import popup from "./popup.js";

const key = "8a2da73517436d7eb5e32d42718fd7e6";
const token = "f1e8aedcec3a974f3827a97d33a226aa9977dfb4bd3d6032cbfd3cd61d678b94";

// boardId which we have added previously in params;
var url_string = window.location.href;
var url = new URL(url_string);
var boardId = url.searchParams.get('id');
var boardName = url.searchParams.get("name");
// console.log(boardName)

//set board name
const boardNameContainer = document.querySelector(".boardName");
boardNameContainer.innerHTML = `<h3>${boardName}</h3>`;

// reload all the lists present on the server(created priviously)
window.addEventListener("load", async function () {
    const promise = await fetch(`https://api.trello.com/1/boards/${boardId}/lists?key=${key}&token=${token}`, { method: 'GET' }) // Get Lists on a Board
    const listArrData = await promise.json();
    console.log(listArrData)
    listArrData.forEach(async function (listObj) {
        createList(listObj.name, listObj.id);
        console.log(listObj.name, listObj.id)
        const promise = await fetch(`https://api.trello.com/1/lists/${listObj.id}/cards?key=${key}&token=${token}`, { method: 'GET' }) // Get Cards in a List
        const cardArrData = await promise.json();
        // console.log(cardArrData)
        cardArrData.forEach(async function (cardObj) {
            createCard(cardObj.name, cardObj.id, listObj.id);
        })
    });

})

//add event listener on addlistbutton and plus sign

async function eventListenerCreateList(){
    const method = "POST";
    const url = `https://api.trello.com/1/lists?key=${key}&token=${token}&idBoard=${boardId}&name=`;//create list
    const action = "createList";
    commonCreateFunction(url, method, action, 'List Name');
}

const addListButton = document.querySelector(".addList");
const navPlus = document.querySelector(".navPlus");

navPlus.addEventListener("click", eventListenerCreateList);
addListButton.addEventListener("click", eventListenerCreateList);

//event delegation on list container

const listContainer = document.querySelector(".list-container");

listContainer.addEventListener("click", async function (e) {
    if (e.target.className === "addCard" || e.target.parentElement.className === "addCard") {
        let listId = e.target.parentElement.id;
        if (e.target.parentElement.className === "addCard") {
            listId = e.target.parentElement.parentElement.id;
        }
        const method = "POST";
        const url = `https://api.trello.com/1/cards?key=${key}&token=${token}&idList=${listId}&name=`//create new card
        const action = "createCard";
        commonCreateFunction(url, method, action, "Card Name", listId)
    }
    // For deleting the list
    if (e.target.className.includes("deletelistIcon") || e.target.parentElement.className.includes("deletelistIcon")) {
        let listId = e.target.parentElement.id;
        if(e.target.parentElement.className === "deletelistIcon"){
            listId = e.target.parentElement.parentElement.id;
        }
        // console.log(checkListId)
        const method = "PUT";
        const url = `https://api.trello.com/1/lists/${listId}?key=${key}&token=${token}&closed=true`;
        const action = 'deleteElement';
        commonCreateFunction(url, method, action, "DELETE", listId);
    }
    //  when card is clicked
    if (e.target.className === "cardName" || e.target.parentElement.className === "cardName") {
        
        let cardId 
        let cardName
        if(e.target.parentElement.className === "cardName"){
            cardId = e.target.parentElement.id;
            cardName = e.target.innerText; 
        }else {
            cardId = e.target.id;
            cardName = e.target.children[0].innerText;
        }
        // console.log(cardId , cardName)
        //  const cardClass = e.target.className;
        
        const promise = await fetch(`https://api.trello.com/1/cards/${cardId}?key=${key}&token=${token}`, { method: 'GET' });
        const card = await promise.json();

        displayCard(cardName, cardId, card.desc);

        //For adding description :-

        const add = document.querySelector(".add");
        //1) check for the previous description 
        if (card.desc !== "") {
            add.innerText = "Edit";
        }
        //2) adding new description     
        add.addEventListener("click", async function (e) {
            // const cardId = e.target.parentElement.parentElement.parentElement.id;
            const method = "PUT";
            const url = `https://api.trello.com/1/cards/${cardId}?key=${key}&token=${token}&desc=`;
            const action = 'addDescription';
            commonCreateFunction(url, method, action, "Description")
        })
        //For canceling the cardDisplay :-
        const display = document.querySelector(".blackBackground");
        const cancel = document.querySelector(".cancel");
        cancel.addEventListener("click", () => {
            // console.log(display, cancel)
            display.remove();
        })

        //For updating the card :-
        const updateCard = document.querySelector(".update");
        updateCard.addEventListener("click", function () {
            const url = `https://api.trello.com/1/cards/${cardId}?key=${key}&token=${token}&name=`;
            const method = "PUT";
            const action = "updatecardname"
            commonCreateFunction(url, method, action, "Update Card Name", cardId)
        })

        //For deleting the card : - 
        const deleteCard = document.querySelector(".deleteCard");
        //const display = document.querySelector(".blackBackground");
        deleteCard.addEventListener("click", function () {
            const url = `https://api.trello.com/1/cards/${cardId}?key=${key}&token=${token}`;
            const method = "DELETE";
            const action = "deleteElement";
            commonCreateFunction(url, method, action, "DELETE", cardId, display);
        })


        //For creating checklist : - 

        // 1) It will get all the previous checklists and its items from the server and display on card
        const checkListIdArr = card.idChecklists;
        checkListIdArr.forEach(async function (checkListId) {

            const promise = await fetch(`https://api.trello.com/1/checklists/${checkListId}?key=${key}&token=${token}`);//Get a Checklist
            const checkList = await promise.json();

            createCheckList(checkList.name, checkList.id);

            const listItemArr = checkList.checkItems;
            listItemArr.forEach(async function (listItem) {
                createItem(listItem.name, listItem.id, checkListId)
            })
        })
        // 2) It will create new checklists on server and display on card
        const checklist = document.querySelector(".checklist");
        checklist.addEventListener("click", async function () {
            const method = "POST";
            const url = `https://api.trello.com/1/checklists?key=${key}&token=${token}&idCard=${cardId}&name=`;
            const action = 'createchecklist';
            commonCreateFunction(url, method, action, "Check List")
        })

        // Adding event listener on the checklist container :-

        const checklistContainer = document.querySelector(".cardDescriptionCheckLists");

        checklistContainer.addEventListener("click", async function (e) {
            // For creating items in checklist 
            if (e.target.className === "addItem" || e.target.parentElement.className === "addItem") {
                let checkListId;
                if (e.target.className === "addItem") {
                    checkListId = e.target.parentElement.id;
                } else {
                    checkListId = e.target.parentElement.parentElement.id;
                }
                const method = "POST";
                const url = `https://api.trello.com/1/checklists/${checkListId}/checkItems?key=${key}&token=${token}&name=`;
                const action = 'createitem';
                commonCreateFunction(url, method, action, "Item Name");
            }
            // For deleting the checklist
            if (e.target.className === "delete") {
                let checkListId = e.target.parentElement.parentElement.id;
                // console.log(checkListId)
                const method = "DELETE";
                const url = `https://api.trello.com/1/checklists/${checkListId}?key=${key}&token=${token}`;
                const action = 'deleteElement';
                commonCreateFunction(url, method, action, "DELETE", checkListId);
            }
        })





    }
})


//common function which will call popup function and then send post requests and create list and card on server and on the ui with user given name.

const commonCreateFunction = async function (URL, Method, action, title, id, deletedCardDisplay) {
    popup(title);
    const text = document.querySelector(".popup input");
    const add = document.querySelector(".popup button");
    const cancel = document.querySelector(".popup i");
    const popupDiv = document.querySelector(".popup")
    cancel.addEventListener("click", () => {
        popupDiv.remove();
    })

    add.addEventListener("click", async function (e) {
        e.preventDefault();
        if (action === 'deleteElement') {
            if (text.value === "DELETE") {
                const promise = await fetch(`${URL}`, { method: Method });
                //const data = await promise.json();
                //console.log(data)
                //let checkListId = data
                const elementToDelete = document.getElementById(`${id}`);
                // console.log(elementToDelete.className)
                if (elementToDelete.className === "cardName") {
                    deletedCardDisplay.remove()
                }
                elementToDelete.remove();
                popupDiv.remove();
            } else {
                popupDiv.remove();
            }
        } else {
            const promise = await fetch(`${URL}${text.value}`, {
                method: Method
            });
            const data = await promise.json();
            if (action === 'createList') {
                createList(data.name, data.id);
                popupDiv.remove();
            }
            if (action === "createCard") {
                createCard(data.name, data.id, id);
                popupDiv.remove();
            }
            if (action === "addDescription") {
                const description = data.desc;
                const descriptionBox = document.querySelector(".descriptionBox");
                descriptionBox.innerText = description;
                popupDiv.remove();
            }
            if (action === "updatecardname") {
                const name = data.name;
                const cardId = document.getElementById(data.id);
                cardId.children[0].innerText = name;
                const nameElement = document.querySelector(".cardnamedisplay");
                nameElement.innerText = name;
                popupDiv.remove();
            }
            if (action === "createchecklist") {
                const name = data.name;
                const id = data.id;
                createCheckList(name, id);
                popupDiv.remove();
            }
            if (action === 'createitem') {
                const name = data.name;
                const id = data.id;
                const checklistId = data.idChecklist;
                createItem(name, id, checklistId);
                popupDiv.remove();
            }
        }


    })
}