import createBoard from "./createboard.js";
import popup from "./popup.js";

const key = "8a2da73517436d7eb5e32d42718fd7e6";
const token = "f1e8aedcec3a974f3827a97d33a226aa9977dfb4bd3d6032cbfd3cd61d678b94";

//reload all the boards present on the server
window.addEventListener('load', async function () {
    const promise = await fetch(`https://api.trello.com/1/members/dhaneshyadav/boards/?key=${key}&token=${token}`);
    const boardArr = await promise.json();
    // console.log(data)
    boardArr.forEach((board) => {
        // console.log(e)
        createBoard(board.name, board.id)
    })
})

//call popup function on clicking createBoardButton and create board on server and on the ui with user given name.
let createboard = document.querySelector(".createBoard");
let plus = document.querySelector(".navPlus");
//for creating new board
createboard.addEventListener("click", create);
plus.addEventListener("click", create);

function create() {
    popup("Board Name");
    const text = document.querySelector(".popup input");
    const save = document.querySelector(".popup button");
    const cancel = document.querySelector(".popup i");
    const popupDiv = document.querySelector(".popup")
    cancel.addEventListener("click", () => {
        popupDiv.remove();
    })
    save.addEventListener("click", async function () {
        const promise = await fetch(`https://api.trello.com/1/boards/?key=${key}&token=${token}&name=${text.value}`, { method: 'POST' });
        const data = await promise.json();
        createBoard(data.name, data.id);
        popupDiv.remove();
    })
}


let boardContainer = document.querySelector(".board-container");


boardContainer.addEventListener("click", async function (e) {
    // e.preventDefault()
    //for moving to the board page.
    if (e.target.className === "heading" || e.target.className === "board") {
        console.log(e.target.lastChild.innerText)
        let boardId = e.target.id;
        let boardName = e.target.lastChild.innerText; 
        if (e.target.className === "heading") {
            boardId = e.target.parentElement.id;
            boardName = e.target.innerText;
        }
        // const promise = await fetch(`https://api.trello.com/1/boards/${boardId}/lists?key=${key}&token=${token}`, { method: 'GET' });
        // const data = await promise.json();
        // console.log(data)
        location.href = `board.html?id=${boardId}&name=${boardName}`;
    }
    //for deleting the board.
    //console.log(e.target.className.includes("delete"))
    if(e.target.className.includes("deleteIcon")) {
        // console.log(e.target.parentElement.id)
        popup("'DELETE'");
        const boardId = e.target.parentElement.id;
        const text = document.querySelector(".popup input");
        const save = document.querySelector(".popup button");
        const cancel = document.querySelector(".popup i");
        const popupDiv = document.querySelector(".popup")
        cancel.addEventListener("click", () => {
        popupDiv.remove();
        })
        save.addEventListener("click", async function (e) {
            e.preventDefault();
            // console.log(text.value)
            if(text.value === "DELETE"){
                // console.log("yes")
                const promise = await fetch(`https://api.trello.com/1/boards/${boardId}?key=${key}&token=${token}`, { method: 'DELETE' });
                // const data = await promise.json();
                const boardToDelete = document.getElementById(`${boardId}`);
                boardToDelete.remove();
                popupDiv.remove();
            }else{
                popupDiv.remove();
            }
        })
      
        // const promise = await fetch(`https://api.trello.com/1/boards/{id}?key=${key}&token=${token}`);
        // const data = await promise.json();

    }
})





// createBoard("app")